<?php namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Requests\SearchRequest;

use App\Models\Post;
use App\Models\Tag;
use App\Models\Comment;
use App\Models\User;

class BlogController extends Controller {

	/**
	 * The pagination number.
	 *
	 * @var int
	 */
	protected $nbrPages;

	/**
	 * Create a new BlogController instance.
	 *
	 * @return void
	*/
	public function __construct()
	{
		$this->nbrPages = 2;

		$this->middleware('redac', ['except' => ['indexFront', 'show', 'tag', 'search']]);
		$this->middleware('admin', ['only' => 'updateSeen']);
		$this->middleware('ajax', ['only' => ['updateSeen', 'updateActive']]);
	}	

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function indexFront()
	{
        $posts = Post::select('id', 'created_at', 'updated_at', 'title', 'slug', 'user_id', 'summary')
                        ->whereActive(true)
                        ->with('user')
                        ->latest();

        $posts = $posts->paginate($this->nbrPages);
		$links = $posts->render();

		return view('front.blog.index', compact('posts', 'links'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Redirection
	 */
	public function index()
	{
		return redirect(route('blog.order', [
			'name' => 'posts.created_at',
			'sens' => 'asc'
		]));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param  Illuminate\Http\Request $request
	 * @return Response
	 */
	public function indexOrder(Request $request)
	{
		$user_id 	= session('statut') == 'admin' ? null : $request->user()->id;
		$orderBy 	= $request->name ? $request->name : 'creted_at';
		$direction	= $request->sens ? $request->sens : 'desc';

        $query = Post::select('posts.id', 'posts.created_at', 'title', 'posts.seen', 'active', 'user_id', 'slug', 'username')
                ->join('users', 'users.id', '=', 'posts.user_id')
                ->orderBy($request->name, $request->sens);

        if ($user_id) {
            $query->where('user_id', $user_id);
        }

        $posts 		= $query->paginate(10);
		
		$links = $posts->appends([
				'name' => $request->name, 
				'sens' => $request->sens
			]);

		if($request->ajax()) {
			return response()->json([
				'view' => view('back.blog.table', compact('statut', 'posts'))->render(), 
				'links' => e($links->setPath('order')->render())
			]);		
		}

		$links->setPath('')->render();

		$order = (object)[
			'name' => $request->name, 
			'sens' => 'sort-' . $request->sens			
		];

		return view('back.blog.index', compact('posts', 'links', 'order'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$url = config('medias.url');
		
		return view('back.blog.create')->with(compact('url'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  App\Http\Requests\PostRequest $request
	 * @return Response
	 */
	public function store(PostRequest $request)
	{

        $post 		= new Post;
        $inputs 	= $request->all();
        $user_id	= $request->user()->id;

        $post->title 		= $inputs['title'];
        $post->summary 		= $inputs['summary'];
        $post->content 		= $inputs['content'];
        $post->slug 		= $inputs['slug'];
        $post->active 		= isset($inputs['active']);

        if ($user_id) {
            $post->user_id = $user_id;
        }
        $post->save();

        // Tags gestion
        if (array_key_exists('tags', $inputs) && $inputs['tags'] != '') {

            $tags = explode(',', $inputs['tags']);

            foreach ($tags as $tag) {
                $tag_ref = Tag::whereTag($tag)->first();
                if (is_null($tag_ref)) {
                    $tag_ref = new Tag;
                    $tag_ref->tag = $tag;
                    $post->tags()->save($tag_ref);
                } else {
                    $post->tags()->attach($tag_ref->id);
                }
            }
        }

		return redirect('blog')->with('ok', trans('back/blog.stored'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  Illuminate\Contracts\Auth\Guard $auth	 
	 * @param  string $slug
	 * @return Response
	 */
	public function show(Guard $auth, $slug)
	{
		$user = $auth->user();
        $post = Post::with('user', 'tags')->whereSlug($slug)->firstOrFail();

        $comments = Comment::wherePost_id($post->id)
                ->with('user')
                ->whereHas('user', function($q) {
                    $q->whereValid(true);
                })
                ->get();

		return view('front.blog.show', compact('post', 'comments', 'user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  App\Repositories\UserRepository $user_gestion
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::with('tags')->findOrFail($id);;
		$url = config('medias.url');
        $tags = [];

        foreach ($post->tags as $tag) {
            array_push($tags, $tag->tag);
        }

		return view('back.blog.edit', compact('url','post', 'tags'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  App\Http\Requests\PostUpdateRequest $request
	 * @param  int  $id
	 * @return Response
	 */
	public function update(PostRequest $request, $id)
	{
		$post 				= Post::find($id);
		$this->authorize('change', $post);
		$user_id 			= null;

		$inputs 			= $request->all();

        $post->title 		= $inputs['title'];
        $post->summary 		= $inputs['summary'];
        $post->content 		= $inputs['content'];
        $post->slug 		= $inputs['slug'];
        $post->active 		= isset($inputs['active']);
        if ($user_id) {
            $post->user_id 	= $user_id;
        }
        $post->save();


        // Tag gestion
        $tags_id = [];
        if (array_key_exists('tags', $inputs) && $inputs['tags'] != '') {

            $tags = explode(',', $inputs['tags']);

            foreach ($tags as $tag) {
                $tag_ref = Tag::whereTag($tag)->first();
                if (is_null($tag_ref)) {
                    $tag_ref = new Tag;
                    $tag_ref->tag = $tag;
                    $tag_ref->save();
                }
                array_push($tags_id, $tag_ref->id);
            }
        }

        $post->tags()->sync($tags_id);

		return redirect('blog')->with('ok', trans('back/blog.updated'));		
	}

	/**
	 * Update "vu" for the specified resource in storage.
	 *
	 * @param  Illuminate\Http\Request $request
	 * @param  int  $id
	 * @return Response
	 */
	public function updateSeen(Request $request, $id)
	{

        $post 		= Post::find($id);
        $inputs		= $request->all();

        $post->seen = $inputs['seen'] == 'true';

        $post->save();

		return response()->json();
	}

	/**
	 * Update "active" for the specified resource in storage.
	 *
	 * @param  Illuminate\Http\Request $request
	 * @param  int  $id
	 * @return Response
	 */
	public function updateActive(Request $request, $id)
	{
		$post 			= Post::find($id);
		$this->authorize('change', $post);
		$inputs			= $request->all();

        $post->active 	= $inputs['active'] == 'true';

        $post->save();

		return response()->json();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Post::find($id);

		$this->authorize('change', $post);

		$post->tags()->detach();
        $post->delete();

		return redirect('blog')->with('ok', trans('back/blog.destroyed'));		
	}

	/**
	 * Find search in blog
	 *
	 * @param  App\Http\Requests\SearchRequest $request
	 * @return Response
	 */
	public function search(SearchRequest $request)
	{
		$search 	= $request->input('search');

        $query 		= Post::select('id', 'created_at', 'updated_at', 'title', 'slug', 'user_id', 'summary')
                        ->whereActive(true)
                        ->with('user')
                        ->latest();

        $posts 		= $query->where(function($q) use ($search) {
                    $q->where('summary', 'like', "%$search%")
                            ->orWhere('content', 'like', "%$search%")
                            ->orWhere('title', 'like', "%$search%");
                })->paginate($this->nbrPages);


		$links = $posts->appends(compact('search'))->render();
		$info = trans('front/blog.info-search') . '<strong>' . $search . '</strong>';
		
		return view('front.blog.index', compact('posts', 'links', 'info'));
	}

}
